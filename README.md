# tasktemplate

tasktemplate is a script that creates a basic skeleton for solving an
algorithmic task. I use it to get quickly started when solving Google CodeJam
or UVa Online Judge tasks.

The skeleton includes a solution file, placeholder input/output files,
placeholder test files and a testing script. There is an option to use a
version control system too.

Supported programming languages:

* C
* C++
* Java
* Python 2.\*
* Python 3.\*

Supported source control systems:

* git
* mercurial


## Installation

Just clone the repository and put the `tasktemplate` script into an executable
path. No additional building is required.


## Usage

Run

    tasktemplate --help

for up-to-date usage information.


## Reporting Bugs

If you have a bug to report, contact me via Bitbucket or send me an email at
<gliktaras@gmail.com>.


## Licensing

This project uses the BSD 2-Clause license.
